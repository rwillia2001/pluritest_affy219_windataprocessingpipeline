﻿using System;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace CSharpToR
{
    class Program
    {
        static void Main(string[] args)
        {
            //location and name of dynamically created R script
            //note: folder must already exist for Streamwriter or exception is thrown
            string pluritestAffyScriptPath = @"D:\pluritest_affy219_windataprocessingpipeline\affymagic.R";

            using (StreamWriter sw = new StreamWriter(pluritestAffyScriptPath))
            {
                sw.WriteLine("affy.headmaster <-function() {");

                sw.WriteLine("library(lumi)");
                sw.WriteLine("library(affy)");
                sw.WriteLine("library(affyPLM)");
                sw.WriteLine("library(Biobase)");
                sw.WriteLine("setwd(\"D:/pluritest_affy219_windataprocessingpipeline/\")");
                sw.WriteLine("sink(file = \"./OUTPUT/GUID/pluritest_log.txt\")");
                sw.WriteLine("celpath <- \"D:/pluritest_affy219_windataprocessingpipeline/INPUT/GUID\"");
                sw.WriteLine("fns <- list.celfiles(path=celpath,full.names=TRUE)");
                sw.WriteLine("fns");

                sw.WriteLine("if (length(fns) < 3) { stop(\"Quality Control outliers step with clustering requires a minimum of 3 samples\")}");

                sw.WriteLine("outpath <- \"./OUTPUT/GUID/\"");
                sw.WriteLine("scratchpath <- \"./scratch/\"");
                sw.WriteLine("abatch <- ReadAffy(filenames=fns)");

                sw.WriteLine("if (abatch@cdfName != \"HG-U219\") { stop (\"The arrays must be HG-U219\")  }");

                sw.WriteLine("Pset <- fitPLM(abatch)");
                sw.WriteLine("plotdirectives <- paste0(outpath, \"RLEplot.pdf\")");
                sw.WriteLine("pdf(file = plotdirectives)");
                sw.WriteLine("RLE(Pset, main = \"Relative Log Expression for uploaded CEL files\", las = 2)");
                sw.WriteLine("dev.off()");
                sw.WriteLine("basicstats <- RLE(Pset, type = \"stats\")");
                sw.WriteLine("basicstats");
                sw.WriteLine("plotdirectives <- paste0(outpath, \"NUSEplot.pdf\")");
                sw.WriteLine("pdf(file = plotdirectives)");
                sw.WriteLine("NUSE(Pset, main = \"Normalized Unscaled Standard Errors for uploaded CEL files\", las = 2)");
                sw.WriteLine("dev.off()");

                sw.WriteLine("write.exprs.directive <- \"./scratch/Data_constant_rma_pmonly_liwong.txt\"");
                sw.WriteLine("eset <- expresso(abatch, normalize.method = \"constant\", bgcorrect.method = \"rma\", pmcorrect.method = \"pmonly\", summary.method = \"liwong\")");
                sw.WriteLine("write.exprs(eset, file = write.exprs.directive)");

                sw.WriteLine("dat <- read.delim(\"./scratch/Data_constant_rma_pmonly_liwong.txt\", header = T, row.names = 1)");
                sw.WriteLine("check.number.rows <- dim(dat)");

                sw.WriteLine("plotdirectives <-paste0(outpath, \"pluritest_image04.pdf\")");
                sw.WriteLine("pdf(file = plotdirectives)");
                sw.WriteLine("boxplot(log(dat), las = 2)");
                sw.WriteLine("dev.off()");

                sw.WriteLine("plotdirectives <- paste0(outpath, \"pluritest_image05.pdf\")");
                sw.WriteLine("hc <- hclust(as.dist(1 - abs(cor(dat[,]))))");
                sw.WriteLine("pdf(file = plotdirectives)");
                sw.WriteLine("plot(hc, hang = -1, main = \"Clustering of Affy-PLM-rma-transformed samples\", sub = \"distance based on pearson correlations\", xlab = \"\")");
                sw.WriteLine("dev.off()");

                sw.WriteLine("source(\"./RScripts/affyscript.R\")");
                sw.WriteLine("source(\"./RScripts/VirtualArray.R\")");
                sw.WriteLine("runVA(\"./scratch/Data_constant_rma_pmonly_liwong.txt\")");
                sw.WriteLine("sink()");
                sw.WriteLine("} #End of affy.headmaster function");
    
                //sw.WriteLine("q()");
                //sw.Flush();
            }

            var R = new System.Diagnostics.Process();
            R.StartInfo.RedirectStandardOutput = true;
            R.StartInfo.RedirectStandardError = true;
            R.StartInfo.UseShellExecute = false;
            R.StartInfo.FileName = @"C:\Program Files\R\R-3.2.3\bin\R.exe";
            R.StartInfo.Arguments = " --no-restore --no-save < " + pluritestAffyScriptPath;

            var rOutput = new StringBuilder();
            var rOutputLogPath = @"D:\pluritest_affy219_windataprocessingpipeline\R_log.txt";
            try
            {
                R.Start();
                while (!R.HasExited)
                {
                    rOutput.Append(R.StandardOutput.ReadToEnd());
                    rOutput.Append(R.StandardError.ReadToEnd());
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            finally
            {
                if (!R.HasExited)
                    R.CloseMainWindow();

                File.AppendAllText(rOutputLogPath, rOutput.ToString());
            }

            Console.WriteLine("Done...");
            Console.ReadLine();
        }
    }
}
