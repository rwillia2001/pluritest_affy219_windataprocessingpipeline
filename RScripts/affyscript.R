

makeAffyVA<-function(Affy,matchIds=selmatchaffy){
  
M<-Affy[matchIds[!is.na(matchIds)],]  
rownames(M)<-rownames(FData[!is.na(matchIds),])
lumiSeq<-new("LumiBatch", exprs = as.matrix(M), se.exprs =as.matrix(M))
fData(lumiSeq)<-FData[!is.na(matchIds),]
lumiSeq
}


runVA<-function(affyout,all=TRUE,wDir=getwd()){
  
  load(file = "./RData/shiftAffy.Rdata")
  load(file="./RData/selmatchaffy.Rdata")
  oldDir<-getwd()
  setwd(wDir)
  liwong<-read.csv(affyout,sep="\t",row.names = 1)
  lumiVA<-makeAffyVA(Affy=liwong,selmatchaffy)
  res<- pluritestAllBatch1(working.lumi = lumiVA,normalize = "quantile",transform = FALSE,Wpluri = W15,Wnov = W12,WpluriCor = cbind(W15,shiftAffy),WnovCor = cbind(W12,shiftAffy),techIndex = 1)
  pluritest.pdf.directive <- paste0(outpath, "pluritest_image06.pdf")
  pdf(file=pluritest.pdf.directive)
  image(y = c(-129:70), x = c((1:200)/50),z= background129_70x1_4,col=color.palette(50),xlab = "novelty", ylab = "pluripotency")
  points(res$corrected$pluri~res$corrected$novel)
  dev.off()
  pluritest.csv.directive <- paste0(outpath, "resultsaffy.csv")
  write.csv(data.frame(names=sampleNames(lumiVA),pluriClassic=res$classic$pluri,novelClassic=res$classic$novel,pluriCor=res$corrected$pluri,novelCor=res$corrected$novel ),file=pluritest.csv.directive)
}
  